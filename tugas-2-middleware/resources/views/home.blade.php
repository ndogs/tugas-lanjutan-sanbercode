@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    You are logged in!
                </div>
                <a href="/route-1" class="btn">Go to Route 1</a>
                <a href="/route-2" class="btn">Go to Route 2</a>
                <a href="/route-3" class="btn">Go to Route 3</a>
            </div>
        </div>
    </div>
</div>
@endsection
