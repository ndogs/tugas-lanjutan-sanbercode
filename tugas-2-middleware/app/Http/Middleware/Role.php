<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->getRole() === 0 && $request->route()->named('route3')) {
            return $next($request);
        } else if (Auth::user()->getRole() === 1 && !$request->route()->named('route1')) {
            return $next($request);
        } else if(Auth::user()->getRole() == 2){
            return $next($request);
        }
        abort(403);
    }
}
