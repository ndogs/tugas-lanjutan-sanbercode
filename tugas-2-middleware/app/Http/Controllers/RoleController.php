<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function route1(){
    	return view('page-1');
    }

    public function route2(){
    	return view('page-2');
    }

    public function route3(){
    	return view('page-3');
    }
}
