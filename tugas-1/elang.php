<?php 

require_once('hewan.php');
require_once('fight.php');

/**
 * Kelas Elang
 */
class Elang
{

	use Hewan,Fight;
	
	function __construct($nama){
		$this->nama = $nama;
		$this->jumlahKaki = 2;
		$this->keahlian = "terbang tinggi";
		$this->attackPower = 10;
		$this->defencePower = 5;
	}

	function getInfoHewan(){
		echo "Nama Hewan : " . $this->nama;
		echo "\nJenis Hewan : Elang";
		echo "\nJumlah Kaki : " . $this->jumlahKaki;
		echo "\nKeahlian : " . $this->keahlian;
		echo "\nDarah : " . $this->darah;
		echo "\nAttack Power : " . $this->attackPower;
		echo "\nDefence Power : " . $this->defencePower;
	}
}
