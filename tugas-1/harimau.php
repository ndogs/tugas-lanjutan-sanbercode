<?php 

require_once('hewan.php');
require_once('fight.php');

/**
 * Kelas Harimau
 */
class Harimau
{

	use Hewan,Fight;
	
	function __construct($nama){
		$this->nama = $nama;
		$this->jumlahKaki = 4;
		$this->keahlian = "lari cepat";
		$this->attackPower = 7;
		$this->defencePower = 8;
	}

	function getInfoHewan(){
		echo "Nama Hewan : " . $this->nama;
		echo "\nJenis Hewan : Harimau";
		echo "\nJumlah Kaki : " . $this->jumlahKaki;
		echo "\nKeahlian : " . $this->keahlian;
		echo "\nDarah : " . $this->darah;
		echo "\nAttack Power : " . $this->attackPower;
		echo "\nDefence Power : " . $this->defencePower;
	}
}