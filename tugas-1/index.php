<?php

require_once('elang.php');
require_once('harimau.php');

$elang_1 = new Elang("elang_1");
$harimau_1 = new Harimau("harimau_1");

$elang_1->getInfoHewan();
echo "\n\n";
$harimau_1->getInfoHewan();
echo "\n\n---------------------------------------------------------------\n";
$elang_1->atraksi();
echo "\n";
$harimau_1->atraksi();
echo "\n\n---------------------------------------------------------------\n";
$elang_1->serang($harimau_1);
echo "\n";
$harimau_1->serang($elang_1);
echo "\n";