<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

	<title>Book Ticket Now</title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-8 mx-auto my-5">
				<div class="card">
					<div class="card-body">
						<h1 class="text-center mb-2">Ndogskun Cinema</h1>
						<div class="text-center">
							<img src="{{ asset('img/cinema.png') }}" alt="Popcorn" style="height:200px;" class="mb-4">
						</div>
						<div id="app">
							<div class="row">
								<div class="col-md-9" style="border-right: 1px solid #ABBFC5">
									<h5>Reservation</h5>
									<hr>
									<div class="row">
										<div v-for="(seat, index) in studio">
											<div class="col-md-1 m-3">
												<button class="btn btn-success" v-on:click="reserve(seat)" v-bind:class="{'btn-danger' : !seat.is_reserved}">@{{ seat.number }}</button>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<h5>Add new seat</h5>
									<hr>
									<div class="row">
										<div class="col-md-12">
											<button class="btn btn-primary" v-on:click="addSeat()">Add new seat</button>
										</div>
									</div>
									<hr>
									<h6 class="mt-4">Delete last seat</h6>
									<hr>
									<div class="row">
										<div class="col-md-12">
											<button class="btn btn-danger" v-on:click="deleteSeat()">Delete last seat</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>
	<script>
		new Vue({
			el : "#app",
			data : {
				studio : []
			},
			methods : {
				addSeat : function(){
					let data = {
						'number' : this.studio.length + 1,
						'is_reserved' : false
					}
					// request for adding seat
					this.$http.post('api/seat', data).then(response => {
						this.studio.push(response.body.data);
					});
				},
				deleteSeat : function(){
					let popped = this.studio.pop();
					// Request for deleting seat
					this.$http.delete('api/seat/' + popped.id).then(response => {});
				},
				reserve : function(seat){
					// Request for reserving seat
					this.$http.post('api/seat/reserve/' + seat.id, {is_reserved : seat.is_reserved}).then(response => {});
					seat.is_reserved = !seat.is_reserved
				}
			},
			mounted : function(){
				// GET /someUrl
				this.$http.get('api/seat').then(response => {

				    // get body data
				    this.studio = response.body.data;

				});
			}
		})
	</script>

</body>
</html>