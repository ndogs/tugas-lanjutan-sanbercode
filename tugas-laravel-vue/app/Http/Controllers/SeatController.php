<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Seat;
use App\Http\Resources\SeatResource;

class SeatController extends Controller
{

    public function index()
    {
        $seats = Seat::all();

        return SeatResource::collection($seats);
    }

    public function store(Request $request)
    {
        $seat = Seat::create([
            'number' => $request->number,
            'is_reserved' => $request->is_reserved
        ]);

        return new SeatResource($seat);
    }

    public function reserve(Request $request, Seat $seat){
        $seat->update([
            'is_reserved' => !$request->is_reserved
        ]);

        return new SeatResource($seat);
    }

    public function destroy(Seat $seat)
    {
        $seat->delete();
        return response()->json(['success']);
    }

}
