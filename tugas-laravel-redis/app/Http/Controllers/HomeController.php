<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\BelajarRedis;

class HomeController extends Controller
{
	public function redis(){
		$sentence = Cache::remember('sentence', 10 * 60 , function(){
			return BelajarRedis::all();
		});
		foreach ($sentence as $word) {
			echo "$word->item ";
		}
	}

	public function query(){
		$sentence = BelajarRedis::all();
		foreach ($sentence as $word) {
			echo "$word->item ";
		}
	}
}
