<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BelajarRedis extends Model
{
    protected $table = 'belajar_redis';
    protected $guarded = [];
}
